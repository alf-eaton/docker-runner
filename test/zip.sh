#!/usr/bin/env bash

cd test/data
zip -r - . | curl -F data=@- -F command="python code.py" http://localhost:8080
