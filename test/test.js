const fs = require('fs')
const os = require('os')
const path = require('path')
const docker = require('../app/docker')

const res = fs.createWriteStream('./output.log', { flags: 'w' })

docker(path.resolve(__dirname + '/data'), 'python code.py', res)
