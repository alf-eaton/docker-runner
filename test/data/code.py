import csv

with open('data/test.csv') as csvfile:
  reader = csv.DictReader(csvfile)
  for row in reader:
    print(row['name'], row['age'])
