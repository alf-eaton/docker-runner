if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}

const express = require('express')
const cors = require('cors')
const multer = require('multer')
const fs = require('fs')
const os = require('os')
const path = require('path')
const Zip = require('adm-zip')
const jwt = require('express-jwt')
const docker = require('./docker')

const { JWT_SECRET, JWT_AUDIENCE } = process.env

const app = express()
app.use(cors())

if (JWT_SECRET && JWT_AUDIENCE) {
  app.use(jwt({
    secret: JWT_SECRET,
    audience: JWT_AUDIENCE
  }))
}

const upload = multer()

if (!fs.existsSync('runs')) {
  fs.mkdirSync('runs')
}

app.post('*', upload.single('data'), (req, res, next) => {
  const dir = path.resolve(fs.mkdtempSync(path.join('runs', 'docker-')))

  const zip = new Zip(req.file.buffer)

  zip.extractAllTo(dir, false)

  docker(dir, req.body.command, res)
})

const port = process.env.PORT || 8080

app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}`)
})
