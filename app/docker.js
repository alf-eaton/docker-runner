const { spawn } = require('child_process')
const uuid = require('uuid/v4')

module.exports = (tempdir, command, res) => {
  const tag = uuid()

  console.log('Building…');

  const ps = spawn('docker', ['build', tempdir, '-t', tag])

  const sendMessage = (type, message) => {
    const msg = String(message)
    res.write(JSON.stringify({ type, msg }) + "\n")
  }

  const sendCode = (code) => {
    console.log(`Finished with code ${code}`);
    sendMessage('code', code)
  }

  ps.stdout.on('data', data => {
    console.log(data.toString())
    sendMessage('docker', data)
  });

  ps.stderr.on('data', data => {
    console.log(data.toString())
    sendMessage('docker-error', data)
  });

  ps.on('close', (code) => {
    sendCode(code)

    if (code !== 0) {
      return
    }

    console.log('Executing…');

    const ps = spawn('docker', [
      'run',
      '--rm',
      // '--tty',
      // '--interactive',
      '--mount',
      `type=bind,source=${tempdir},target=/data`,
      '--workdir',
      '/data',
      tag,
      'bash',
      '-c',
      command,
    ])

    ps.stdout.on('data', data => {
      console.log(data.toString())
      sendMessage('output', data)
    });

    ps.stderr.on('data', data => {
      console.log(data.toString())
      sendMessage('output-error', data)
    });

    ps.on('close', (code) => {
      sendCode(code)

      res.end()

      //  TODO: remove the temp dir
    })
  });
}
